/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.robotproject;

/**
 *
 * @author user1
 */
public class Fuel extends Obj {

    int volumn;

    public Fuel(int x, int y, int volumn) {
        super('F', x, y);
        this.volumn = volumn;
    }

    public int fillFuel() {
        int vol = volumn;
        symbol = '-';
        volumn = volumn;
        return volumn;
    }

    public int getVolumn() {
        return volumn;
    }
    

}
