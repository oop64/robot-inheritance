/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.robotproject;

import java.util.Scanner;

/**
 *
 * @author user1
 */
public class MainProgram {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        
        TableMap map = new TableMap(20, 20);
        Robot robot = new Robot(2, 2, 'x', map, 10);
        Bomb bomb = new Bomb(5, 5);
        
        map.setBomb(bomb);
        
        map.addObj(new Tree(10,10));
        map.addObj(new Tree(9,10));
        map.addObj(new Tree(10,9));
        map.addObj(new Tree(11,10));
        map.addObj(new Tree(5,10));
        map.addObj(new Tree(15,15));
        map.addObj(new Tree(9,15));
        map.addObj(new Tree(15,9));
        map.addObj(new Tree(11,15));
        map.addObj(new Tree(5,15));
        
        map.addObj(new Fuel(0,5,20));
        map.addObj(new Fuel(15,15,20));
        map.addObj(new Fuel(17,6,20));
        
        map.setRobot(robot);
        
        while (true) {
            map.showMap();//WASD;Q=out
            char direction = inputDirection(kb);
            if(direction == 'q'){
                ByeBye();
                break; 
            }
            robot.walk(direction);
        }

    }

    private static void ByeBye() {
        System.out.println("Bye Bye!!!");
    }

    private static char inputDirection(Scanner kb) {
        String str = kb.next();
        return str.charAt(0); 
    }
}
