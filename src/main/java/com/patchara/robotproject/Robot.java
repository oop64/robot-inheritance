/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.robotproject;

/**
 *
 * @author user1
 */
public class Robot extends Obj {

    private TableMap map;
    int fuel;

    public Robot(int x, int y, char symbol, TableMap map, int fuel) {
        super(symbol, x, y);
        this.map = map;
        this.fuel = fuel;
    }

    public boolean walk(char direction) {
        switch (direction) {
            case 'w':
                if (walkN()) {
                    return false;
                }
                break;
            case 's':
                if (wakkS()) {
                    return false;
                }
                break;
            case 'd':
                if (walkE()) {
                    return false;
                }
                break;
            case 'a':
                if (walkW()) {
                    return false;
                }
                break;
            default:
                return false;
        }
        checkBomb();
        return true;
    }

    private void checkBomb() {
        if (map.isBomb(x, y)) {
            System.out.println("FOUND BOMB!!! (" + x + "," + y + ")");
        }
    }

    private boolean canWalk(int x, int y) {
        return fuel>0 && map.inMap(x, y) && !map.isTree(x, y);
    }

    private void reduceFuel() {
        fuel--;
    }

    private boolean walkW() {
        checkFuel();
        if (canWalk(x - 1, y)) {
            x -= 1;
        } else {
            return true;
        }
        reduceFuel();
        return false;
    }

    private boolean walkE() {
        checkFuel();
        if (canWalk(x + 1, y)) {
            x += 1;
        } else {
            return true;
        }
        reduceFuel();
        return false;
    }

    private boolean wakkS() {
        checkFuel();
        if (canWalk(x, y + 1)) {
            y += 1;
        } else {
            return true;
        }
        reduceFuel();
        return false;
    }

    private boolean walkN() {
        checkFuel();
        if (canWalk(x, y - 1)) {
            y -= 1;
        } else {
            return true;
        }
        reduceFuel();
        return false;
    }
    
        private void checkFuel() {
        int fuel = map.fillFuel(x, y);
        if(fuel>0){
            this.fuel+=fuel;
        }
    }

    @Override
    public String toString() {
        return "Robot>>Obj{x = " + x + ", y = " + y + ", symbol = " + symbol + "} fuel = " + fuel;

    }
}
